package com.anas.fishday.screens.main.fragments.cart;

import com.anas.fishday.entities.BaseEntity;
import com.anas.fishday.entities.Order;
import com.anas.fishday.entities.OrderItem;
import com.anas.fishday.entities.RequestEntity;
import com.anas.fishday.entities.ResponseEntity;
import com.anas.fishday.screens.main.fragments.cart.interfaces.CartInteractor;
import com.anas.fishday.screens.main.fragments.cart.interfaces.CartPresenter;

import java.util.List;

/**
 * Created by Anas on 2/27/2018.
 */

public class CartPresenterImpl implements CartPresenter {


    private CartInteractor interactor;
    private CartModel model;

    public CartPresenterImpl(CartInteractor interactor) {
        this.interactor = interactor;
        model = new CartModel(this);
    }

    @Override
    public void getCart() {
        model.getCart();
    }

    @Override
    public void onGettingCartSucceed(ResponseEntity<Order> cartResponseEntity) {
        String status  = cartResponseEntity.getStatus();
        switch (status){
            case "success":
                interactor.showCart(cartResponseEntity);
                break;
            case "fail":
                interactor.showErrorForGettingCart(cartResponseEntity.getMessage());
                break;
        }
    }

    @Override
    public void onGettingCartFailed(String message) {
        interactor.showErrorForGettingCart(message);
    }

    @Override
    public void placeOrder(int orderId, Order order) {
        model.placeOrder(orderId, order);
    }

    @Override
    public void onPlacingOrderSucceed(ResponseEntity<Order> cartResponseEntity) {
        String status  = cartResponseEntity.getStatus();
        switch (status){
            case "success":
                interactor.showSuccessForPlacingOrder(cartResponseEntity);
                break;
            case "fail":
                interactor.showErrorForPlacingOrder(cartResponseEntity.getMessage());
                break;
        }

    }

    @Override
    public void onPlacingOrderFailed(String message) {
        interactor.showErrorForPlacingOrder(message);
    }

    @Override
    public void deleteOrderItem(int orderItemId) {
        model.deleteOrderItem(orderItemId);
    }

    @Override
    public void onDeleteOrderItemSucceed(ResponseEntity<BaseEntity> cartResponseEntity) {
        String status  = cartResponseEntity.getStatus();
        switch (status){
            case "success":
                interactor.showSuccessForDeletingOrderItem(cartResponseEntity);
                break;
            case "fail":
                interactor.showErrorForDeletingOrderItem(cartResponseEntity.getMessage());
                break;
        }
    }

    @Override
    public void onDeleteOrderItemFailed(String message) {
        interactor.showErrorForDeletingOrderItem(message);
    }

    @Override
    public void onStop() {
        model.clearDisposable();
    }
}
