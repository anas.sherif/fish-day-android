package com.anas.fishday.screens.orderstatus;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.anas.fishday.BR;
import com.anas.fishday.R;
import com.anas.fishday.base.FishDayActivity;
import com.anas.fishday.databinding.ActivityOrderStatusBinding;
import com.anas.fishday.storage.FishDayStorage;

public class OrderStatusActivity extends FishDayActivity {

    private ActivityOrderStatusBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_order_status);
        binding.setVariable(BR.order, FishDayStorage.getOrder());
        binding.orderInfo.setVariable(BR.order, FishDayStorage.getOrder());
        initToolbar();
    }

    private void initToolbar() {
        Toolbar toolbar = binding.toolbar.toolbar;
//        AppBarLayout appBarLayout = binding.toolbar.appBarLayout;
//        toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
        binding.toolbar.toolbar.setTitle(R.string.order_status);
        binding.toolbar.cartLayout.setVisibility(View.GONE);
//        appBarLayout.setBackgroundColor(ContextCompat.getColor(this, android.R.color.transparent));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
}
