package com.anas.fishday.screens.main.activity.interfaces;

import com.anas.fishday.entities.Order;

/**
 * Created by Anas on 2/15/2018.
 */

public interface MainInteractor {

    void showErrorOnGettingLastOrder(String message);
    void handleGettingLastOrder(Order order);
}
