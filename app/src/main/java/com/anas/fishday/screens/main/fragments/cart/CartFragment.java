package com.anas.fishday.screens.main.fragments.cart;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.anas.fishday.BR;
import com.anas.fishday.R;
import com.anas.fishday.app.FishDayApplication;
import com.anas.fishday.base.FishDayActivity;
import com.anas.fishday.base.FishDayFragment;
import com.anas.fishday.databinding.FragmentCartBinding;
import com.anas.fishday.entities.BaseEntity;
import com.anas.fishday.entities.Order;
import com.anas.fishday.entities.OrderItem;
import com.anas.fishday.entities.ResponseEntity;
import com.anas.fishday.screens.cartactivity.CartActivity;
import com.anas.fishday.screens.main.activity.MainActivity;
import com.anas.fishday.screens.main.fragments.cart.adapters.CartAdapter;
import com.anas.fishday.screens.main.fragments.cart.interfaces.CartInteractor;
import com.anas.fishday.screens.main.fragments.cart.interfaces.CartPresenter;
import com.anas.fishday.screens.main.fragments.cart.interfaces.OnOrderItemClickListener;
import com.anas.fishday.screens.main.fragments.completeorder.CompleteOrderFragment;
import com.anas.fishday.storage.FishDayStorage;
import com.anas.fishday.utils.ImageDownloader;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CartFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CartFragment extends FishDayFragment implements CartInteractor,
        OnOrderItemClickListener, View.OnClickListener {


    private CartPresenter presenter;
    private FishDayActivity fishDayActivity;
    private FragmentCartBinding binding;
    private Order order;
    private int orderId;

    private RecyclerView.LayoutManager productsLayoutManager;
    private CartAdapter cartAdapter;

    private ImageDownloader imageDownloader;
    private double subTotal;
    private double tax;
    private double total;

    public CartFragment() {
        // Required empty public constructor
    }

    public static CartFragment newInstance() {
        CartFragment fragment = new CartFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fishDayActivity = (FishDayActivity) getActivity();
        imageDownloader = FishDayApplication.getWebServiceComponent().getImageDownloader();
        presenter = new CartPresenterImpl(this);
        orderId = FishDayStorage.getOrder().getId();
        if (getActivity() instanceof MainActivity)
            ((MainActivity) getActivity()).toolbar.setTitle(R.string.cart);
        else if (getActivity() instanceof CartActivity)
            ((CartActivity) getActivity()).toolbar.setTitle(R.string.cart);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil
                .inflate(inflater, R.layout.fragment_cart, container, false);
        binding.setVariable(BR.order, order);
        binding.setVariable(BR.cartOnClickListener, this);
        binding.executePendingBindings();
        initCartRecyclerView();
        getCart();
        return binding.getRoot();
    }

    private void initCartRecyclerView() {
        cartAdapter = new CartAdapter(fishDayActivity, imageDownloader, this);
        productsLayoutManager = new LinearLayoutManager(fishDayActivity);
        binding.cartRecyclerView.setLayoutManager(productsLayoutManager);
        binding.cartRecyclerView.setAdapter(cartAdapter);
    }

    private void getCart() {
        fishDayActivity.showProgressDialog();
        presenter.getCart();
    }

    private void deleteOrderItem(int orderItemId) {
        presenter.deleteOrderItem(orderItemId);
    }

    @Override
    public void onIncreaseQuantityClick(OrderItem orderItem, int index) {
        int newQuantity = orderItem.getQuantity() + 1;
        double newTotalPrice = newQuantity * Double.parseDouble(orderItem.getUnitPrice());
        orderItem.setQuantity(newQuantity);
        orderItem.setTotalPrice(String.valueOf(newTotalPrice));
        cartAdapter.updateSingleOrderItem(index, orderItem);
        updateOrderTotalPrice(orderItem, true);
    }

    @Override
    public void onDecreaseQuantityClick(OrderItem orderItem, int index) {
        if (orderItem.getQuantity() > 1) {
            int newQuantity = orderItem.getQuantity() - 1;
            double newTotalPrice = newQuantity * Double.parseDouble(orderItem.getUnitPrice());
            orderItem.setQuantity(newQuantity);
            orderItem.setTotalPrice(String.valueOf(newTotalPrice));
            cartAdapter.updateSingleOrderItem(index, orderItem);
            updateOrderTotalPrice(orderItem, false);
        }
    }

    @Override
    public void onDeleteItemClick(OrderItem orderItem, int index) {
        deleteOrderItem(orderItem.getId());
        cartAdapter.removeOrderItem(index);
    }

    @Override
    public void showErrorForGettingCart(String message) {
        fishDayActivity.showSnackBar(binding.cartCoordinator, message);
        fishDayActivity.dismissProgressDialog();
    }

    @Override
    public void showCart(ResponseEntity<Order> cartResponseEntity) {
        order = cartResponseEntity.getEntity();
        cartAdapter.setOrderItems(order.getOrderItems());
        fishDayActivity.dismissProgressDialog();
        subTotal = Double.parseDouble(order.getSubTotal());
        total = Double.parseDouble(order.getTotal());
        tax = Double.parseDouble(order.getTax());
        binding.subTotalTv.setText(String.format(getString(R.string.sar_format), order.getSubTotal()));
        binding.taxTv.setText(String.format(getString(R.string.sar_format), order.getTax()));
        binding.totalTv.setText(String.format(getString(R.string.sar_format), order.getTotal()));
    }

    @Override
    public void showErrorForPlacingOrder(String message) {
        Log.e("Place Order", "FAILED");
        fishDayActivity.showSnackBar(binding.cartCoordinator, message);
        fishDayActivity.dismissProgressDialog();
    }

    @Override
    public void showSuccessForPlacingOrder(ResponseEntity<Order> cartResponseEntity) {
//        Log.e("Place Order", "SUCCESS");
//        fishDayActivity.showSnackBar(binding.cartCoordinator, cartResponseEntity.getMessage());
        fishDayActivity.dismissProgressDialog();
        fishDayActivity.replaceFragment(CompleteOrderFragment.newInstance());
    }

    @Override
    public void showErrorForDeletingOrderItem(String message) {
        fishDayActivity.showSnackBar(binding.cartCoordinator, message);
        fishDayActivity.dismissProgressDialog();

    }

    @Override
    public void showSuccessForDeletingOrderItem(ResponseEntity<BaseEntity> cartResponseEntity) {
        fishDayActivity.showSnackBar(binding.cartCoordinator, cartResponseEntity.getMessage());
        fishDayActivity.dismissProgressDialog();

    }

    private void updateOrderTotalPrice(OrderItem orderItem, boolean increase) {
        double unitPrice = Double.parseDouble(orderItem.getUnitPrice());
        if (increase)
            subTotal += unitPrice;
        else
            subTotal -= unitPrice;

        tax = subTotal * 0.05;
        total = subTotal + tax;

        binding.subTotalTv.setText(String.format(getString(R.string.sar_format), String.valueOf(subTotal)));
        binding.taxTv.setText(String.format(getString(R.string.sar_format), String.valueOf(tax)));
        binding.totalTv.setText(String.format(getString(R.string.sar_format), String.valueOf(total)));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.orderNowBtn:
                if (cartAdapter.getOrderItems() != null && cartAdapter.getOrderItems().size() > 0) {
                    if (subTotal >= 100) {
                        Order order = new Order();
                        order.setOrderItems(cartAdapter.getOrderItems());
                        presenter.placeOrder(orderId, order);
                        fishDayActivity.showProgressDialog();
                    } else {
                        fishDayActivity.showSnackBar(binding.cartCoordinator, R.string.error_cart_less_than_100);
                    }
                } else {
                    fishDayActivity.showSnackBar(binding.cartCoordinator, R.string.error_cart_is_empty);
                }
                break;
        }
    }
}
