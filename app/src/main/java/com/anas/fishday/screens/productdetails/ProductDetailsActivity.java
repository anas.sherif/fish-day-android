package com.anas.fishday.screens.productdetails;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.anas.fishday.BR;
import com.anas.fishday.R;
import com.anas.fishday.base.FishDayActivity;
import com.anas.fishday.databinding.ActivityProductDetailsBinding;
import com.anas.fishday.entities.Order;
import com.anas.fishday.entities.Product;
import com.anas.fishday.screens.cartactivity.CartActivity;
import com.anas.fishday.screens.meter.MeterActivity;
import com.anas.fishday.screens.productdetails.adapter.ImagePagerAdapter;
import com.anas.fishday.screens.productdetails.dialog.CreateOrderItemDialogFragment;
import com.anas.fishday.screens.productdetails.interfaces.OnImageClickListener;
import com.anas.fishday.screens.productdetails.interfaces.ProductDetailsInteractor;
import com.anas.fishday.storage.FishDayStorage;
import com.anas.fishday.utils.Constant;

public class ProductDetailsActivity extends FishDayActivity implements ProductDetailsInteractor,
        View.OnClickListener, OnImageClickListener, CreateOrderItemDialogFragment.OnCreateOrderItemSuccessListener {

    private ActivityProductDetailsBinding detailsBinding;
    private Product product;
    private ImagePagerAdapter pagerAdapter;
    private int meterPercentage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        detailsBinding = DataBindingUtil.setContentView(this, R.layout.activity_product_details);

        Bundle bundle = getBundle();
        if (bundle != null) {
            product = (Product) bundle.get(Constant.PRODUCT);
            meterPercentage = bundle.getInt("meterPercentage");
        }
        initToolbar();
        setDataToView();

    }

    private void setDataToView() {
        detailsBinding.setVariable(BR.productDetails, product);
        detailsBinding.setVariable(BR.productDetailsOnClickListener, this);
        detailsBinding.executePendingBindings();

        pagerAdapter = new ImagePagerAdapter(this, product.getImages(), this);
        detailsBinding.imageViewPager.setAdapter(pagerAdapter);

        detailsBinding.pointerSpeedometer.speedPercentTo(meterPercentage);
        detailsBinding.pointerSpeedometer.setOnClickListener(v -> {
            Bundle meterBundle = new Bundle();
            meterBundle.putInt("Meter", meterPercentage);
            startActivity(MeterActivity.class, meterBundle);
        });
    }

    private void initToolbar() {
        Toolbar toolbar = detailsBinding.toolbar.toolbar;
        detailsBinding.toolbar.toolbar.setTitle(product.getName());
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        detailsBinding.toolbar.cartLayout.setVisibility(View.VISIBLE);
        detailsBinding.toolbar.cartLayout.setOnClickListener(view -> startActivity(CartActivity.class));
        try {
            detailsBinding.toolbar.numOfItemsTv.setText(String.valueOf(FishDayStorage.getOrder().getOrderItems().size()));
        }catch (NullPointerException e){
            e.printStackTrace();
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        } else {
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.orderNowBtn:
                CreateOrderItemDialogFragment orderDialogFragment = new CreateOrderItemDialogFragment();
                orderDialogFragment.setOnCreateOrderItemSuccessListener(this);
                Bundle bundle = new Bundle();
                bundle.putSerializable(Constant.PRODUCT, product);
                orderDialogFragment.setArguments(bundle);
                orderDialogFragment.show(getSupportFragmentManager(), "createOrder");
                break;
        }
    }

    @Override
    public void onImageClick(String image) {
        Bundle bundle = new Bundle();
        bundle.putString("image", image);
//        startActivity(ImageViewerActivity.class, bundle);
    }

    @Override
    public void onCreateOrderItemSuccess(Order order) {
        FishDayStorage.saveOrder(order);
        try {
            detailsBinding.toolbar.numOfItemsTv.setText(String.valueOf(order.getOrderItems().size()));
        }catch (NullPointerException e){
            e.printStackTrace();
        }
        startActivity(CartActivity.class);
    }
}
