package com.anas.fishday.screens.confirmregister;

import com.anas.fishday.entities.ResponseEntity;
import com.anas.fishday.entities.User;
import com.anas.fishday.screens.confirmregister.interfaces.ConfirmRegistrationInteractor;
import com.anas.fishday.screens.confirmregister.interfaces.ConfirmRegistrationPresenter;

/**
 * Created by Anas on 2/24/2018.
 */

public class ConfirmRegistrationPresenterImpl implements ConfirmRegistrationPresenter {

    private ConfirmRegistrationInteractor interactor;
    private ConfirmRegistrationModel model;

    public ConfirmRegistrationPresenterImpl(ConfirmRegistrationInteractor interactor) {
        this.interactor = interactor;
        model = new ConfirmRegistrationModel(this);
    }

    @Override
    public void confirmRegistration(User user) {
        model.confirmRegistration(user);
    }

    @Override
    public void onConfirmRegistrationSuccess(ResponseEntity<User> responseEntity) {
        String status = responseEntity.getStatus();
        switch (status){
            case "success":
                User user = responseEntity.getEntity();
                interactor.goToHomeScreen(user);
                break;
            case "fail":
                interactor.showErrorMessage(responseEntity.getMessage());
                break;
        }
    }

    @Override
    public void onConfirmRegistrationFailure(String message) {
        interactor.showErrorMessage(message);
    }

    @Override
    public void onStop() {

    }
}
