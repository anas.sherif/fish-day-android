package com.anas.fishday.screens.main.fragments.about;


import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.anas.fishday.R;
import com.anas.fishday.base.FishDayFragment;
import com.anas.fishday.databinding.FragmentAboutBinding;
import com.anas.fishday.screens.main.activity.MainActivity;

/**
 * A simple {@link Fragment} subclass.
 */
public class AboutFragment extends FishDayFragment {


    private FragmentAboutBinding binding;
    public AboutFragment() {
        // Required empty public constructor
    }


    public static AboutFragment newInstance(){
        AboutFragment aboutFragment = new AboutFragment();
        return aboutFragment;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_about, container, false);

        return binding.getRoot();
    }
    private void initToolbar(){
        ((MainActivity)getActivity()).toolbar.setTitle(R.string.cart);
    }
}
