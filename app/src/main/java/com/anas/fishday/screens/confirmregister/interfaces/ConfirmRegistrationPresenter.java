package com.anas.fishday.screens.confirmregister.interfaces;

import com.anas.fishday.base.BasePresenter;
import com.anas.fishday.entities.ResponseEntity;
import com.anas.fishday.entities.User;

/**
 * Created by Anas on 2/24/2018.
 */

public interface ConfirmRegistrationPresenter extends BasePresenter{

    void confirmRegistration(User user);
    void onConfirmRegistrationSuccess(ResponseEntity<User> responseEntity);
    void onConfirmRegistrationFailure(String message);
}
