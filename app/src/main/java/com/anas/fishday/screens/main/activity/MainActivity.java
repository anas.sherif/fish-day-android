package com.anas.fishday.screens.main.activity;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.anas.fishday.R;
import com.anas.fishday.base.FishDayActivity;
import com.anas.fishday.databinding.ActivityMainBinding;
import com.anas.fishday.entities.ItemMenu;
import com.anas.fishday.entities.Order;
import com.anas.fishday.screens.login.LoginActivity;
import com.anas.fishday.screens.main.activity.interfaces.MainInteractor;
import com.anas.fishday.screens.main.activity.interfaces.MainPresenter;
import com.anas.fishday.screens.main.activity.interfaces.OnMenuSelectedListener;
import com.anas.fishday.screens.main.activity.menu.MenuAdapter;
import com.anas.fishday.screens.main.fragments.about.AboutFragment;
import com.anas.fishday.screens.main.fragments.cart.CartFragment;
import com.anas.fishday.screens.main.fragments.contact.ContactFragment;
import com.anas.fishday.screens.main.fragments.home.HomeFragment;
import com.anas.fishday.screens.main.fragments.myorders.MyOrdersFragment;
import com.anas.fishday.screens.main.fragments.services.ServicesFragment;
import com.anas.fishday.screens.orderstatus.OrderStatusActivity;
import com.anas.fishday.screens.splash.SplashActivity;
import com.anas.fishday.storage.FishDayStorage;
import com.anas.fishday.utils.Constant;
import com.anas.fishday.utils.MenuUtil;

import java.util.List;

public class MainActivity extends FishDayActivity implements MainInteractor, OnMenuSelectedListener,
        HomeFragment.OnFragmentAttachedListener {

    private MainPresenter presenter;
    private ActivityMainBinding binding;
    private MenuAdapter menuAdapter;
    private RecyclerView.LayoutManager layoutManager;
    public Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        presenter = new MainPresenterImpl(this);
        initToolbar();
        initMenuItems();
        showHomeFragment();
        getLastOrder();
    }

    private void getLastOrder() {
        presenter.getLastOrder();
//        if (FishDayStorage.getOrderId() == 0){
//        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        presenter.onStop();
    }

    private void initToolbar() {
        toolbar = binding.appBarMain.toolbar.toolbar;
        toolbar.setTitle(R.string.home);
        toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
        setSupportActionBar(toolbar);

        DrawerLayout drawer = binding.drawerLayout;
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        binding.appBarMain.toolbar.cartLayout.setOnClickListener(view -> handleCartClick());
    }

    private void initMenuItems() {
        List<ItemMenu> itemMenuList = MenuUtil.getMenu(this);
        menuAdapter = new MenuAdapter(itemMenuList, this, this);
        layoutManager = new LinearLayoutManager(this);
        binding.menuRecyclerView.setLayoutManager(layoutManager);
        binding.menuRecyclerView.setAdapter(menuAdapter);
    }

    private void showHomeFragment() {
        HomeFragment homeFragment = HomeFragment.newInstance();
        replaceFragment(homeFragment);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = binding.drawerLayout;
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_cart) {
//            handleCartClick();
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMenuSelected(View view, ItemMenu itemMenu) {
        binding.drawerLayout.closeDrawer(Gravity.START);
        switch (itemMenu.getPosition()) {
            case Constant.MENU_HOME:
                toolbar.setTitle(R.string.home);
                replaceFragment(HomeFragment.newInstance());
                break;
            case Constant.MENU_MY_ORDERS:
                toolbar.setTitle(R.string.my_orders);
                replaceFragmentBackStack(MyOrdersFragment.newInstance());
                break;
            case Constant.MENU_CART:
                handleCartClick();
                break;
            case Constant.MENU_SERVICES:
                toolbar.setTitle(R.string.our_services);
                replaceFragmentBackStack(ServicesFragment.newInstance());
                break;
            case Constant.MENU_ABOUT:
                toolbar.setTitle(R.string.about);
                replaceFragmentBackStack(AboutFragment.newInstance());
                break;
            case Constant.MENU_CONTACT:
                toolbar.setTitle(R.string.contact);
                replaceFragmentBackStack(ContactFragment.newInstance());
                break;
            case Constant.MENU_LANGUAGE:
                if (FishDayStorage.getAppLanguage().equals(Constant.LANGUAGE_EN)) {
                    updateLanguage(Constant.LANGUAGE_AR);
                } else {
                    updateLanguage(Constant.LANGUAGE_EN);
                }
                startActivity(SplashActivity.class);
                finishAffinity();
                break;
            case Constant.MENU_LOGOUT:
                FishDayStorage.clearData();
                startActivity(SplashActivity.class, true);
                break;
            default:
                toolbar.setTitle(R.string.home);
                replaceFragment(HomeFragment.newInstance());
                break;
        }
    }

    private void handleCartClick() {
        if (FishDayStorage.getUser() != null) {
            if (FishDayStorage.getOrder() != null) {
                if (FishDayStorage.getOrder().getStatus() == Constant.ORDER_STATUS_CART) {
                    toolbar.setTitle(R.string.cart);
                    replaceFragmentBackStack(CartFragment.newInstance());
                } else {
                    startActivity(OrderStatusActivity.class);
                }
            } else {
                // order equals null
            }
        } else {
            startActivity(LoginActivity.class, true);
        }
    }

    @Override
    public void showErrorOnGettingLastOrder(String message) {

    }

    @Override
    public void handleGettingLastOrder(Order order) {
        FishDayStorage.saveOrder(order);
        try {
            binding.appBarMain.toolbar.numOfItemsTv.setText(String.valueOf(order.getOrderItems().size()));
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateCartItems(int numOfItems) {
        try {
            binding.appBarMain.toolbar.numOfItemsTv.setText(String.valueOf(numOfItems));
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }
}
