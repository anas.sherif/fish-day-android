package com.anas.fishday.screens.main.fragments.home.adapters;

import android.content.Context;
import android.databinding.BindingAdapter;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.anas.fishday.BR;
import com.anas.fishday.R;
import com.anas.fishday.entities.Image;
import com.anas.fishday.entities.Product;
import com.anas.fishday.screens.main.fragments.home.interfaces.OnProductClickListener;
import com.anas.fishday.utils.ImageDownloader;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Anas on 2/16/2018.
 */

public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ProductsViewHolder> {
    private Context context;
    private List<Product> productList = new ArrayList<>();
    private OnProductClickListener onProductClickListener;
    private static ImageDownloader imageDownloader;

    public ProductsAdapter(Context context, ImageDownloader downloader, OnProductClickListener listener) {
        this.context = context;
        this.imageDownloader = downloader;
        this.onProductClickListener = listener;
        productList.clear();
    }

    @Override
    public ProductsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        ViewDataBinding view = DataBindingUtil
                .inflate(layoutInflater, R.layout.item_product, parent, false);
        return new ProductsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ProductsViewHolder holder, int position) {
        Product product = productList.get(position);
        holder.bind(product, onProductClickListener);
    }

    @Override
    public int getItemCount() {
        return productList.size();
    }

    public void setProductList(List<Product> productList) {
        this.productList.clear();
        this.productList.addAll(productList);
        notifyDataSetChanged();
    }

    @BindingAdapter("loadProductImage")
    public static void loadProductImage(ImageView imageView, List<Image> images) {
        if (images != null && images.size() > 0) {
            imageDownloader.loadImage(images.get(0).getSmall(), imageView);
        }
    }

    class ProductsViewHolder extends RecyclerView.ViewHolder {
        ViewDataBinding viewDataBinding;

        public ProductsViewHolder(ViewDataBinding dataBinding) {
            super(dataBinding.getRoot());
            this.viewDataBinding = dataBinding;
        }

        public void bind(Object obj, OnProductClickListener onProductClickListener) {
            viewDataBinding.setVariable(BR.product, obj);
            viewDataBinding.setVariable(BR.onProductClickListener, onProductClickListener);
        }

    }
}