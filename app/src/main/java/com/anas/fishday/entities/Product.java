package com.anas.fishday.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by Anas on 2/16/2018.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Product extends BaseEntity{

    @JsonProperty("id")
    private int id;
    @JsonProperty("name")
    private String name;
    @JsonProperty("desc")
    private String description;
    @JsonProperty("images")
    private List<Image> images;
    @JsonProperty("kilo_price")
    private String kiloPrice;
    @JsonProperty("piece_price")
    private String piecePrice;
    @JsonProperty("promotion_kilo_price")
    private String promotionKiloPrice;
    @JsonProperty("promotion_piece_price")
    private String promotionPiecePrice;
    @JsonProperty("category")
    private Category category;

    public int getId() {
        return id;
    }

    public String getKiloPrice() {
        return kiloPrice;
    }

    public String getPiecePrice() {
        return piecePrice;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public List<Image> getImages() {
        return images;
    }

    public String getPromotionKiloPrice() {
        return promotionKiloPrice;
    }

    public String getPromotionPiecePrice() {
        return promotionPiecePrice;
    }

    public Category getCategory() {
        return category;
    }
}
