package com.anas.fishday.network;

import com.anas.fishday.entities.BaseEntity;
import com.anas.fishday.entities.Category;
import com.anas.fishday.entities.City;
import com.anas.fishday.entities.Data;
import com.anas.fishday.entities.Meter;
import com.anas.fishday.entities.Offer;
import com.anas.fishday.entities.Order;
import com.anas.fishday.entities.OrderItem;
import com.anas.fishday.entities.Product;
import com.anas.fishday.entities.RequestEntity;
import com.anas.fishday.entities.ResponseEntity;
import com.anas.fishday.entities.User;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.Response;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Anas on 2/15/2018.
 */

public interface AppService {

    /*                     User                    */
    @POST("users/signup")
    Single<ResponseEntity<User>> register(@Body User user);
    @POST("verifications")
    Single<ResponseEntity<User>> confirmRegistration(@Body User user);
    @POST("users/signin")
    Single<ResponseEntity<User>> login(@Body User user);
    @GET("users/create_guest")
    Single<ResponseEntity<User>> registerGuest();
    @GET("cities")
    Single<ResponseEntity<List<City>>> getCities();
    /*                      Home                      */
    @GET("home")
    Single<ResponseEntity<List<Product>>> getProducts(@Query("q[category_id_eq]") int categoryId);

    @GET("meter")
    Single<ResponseEntity<Meter>> getMeter();
    @GET("sliders")
    Single<ResponseEntity<List<Offer>>> getOffers();
    @GET("categories")
    Single<ResponseEntity<List<Category>>> getCategories();
    @GET("products/services")
    Single<ResponseEntity<List<Product>>> getServices();

    /*                      Order                        */
    @GET("orders/last_incomplete_order")
    Single<ResponseEntity<Order>> getLastOrder();

    @POST("order_items/create_or_update")
    Single<ResponseEntity<Order>> createOrderItem(@Body OrderItem orderItem);

    @PUT("orders/{order_id}/update_order_items")
    Single<ResponseEntity<Order>> placeOrder(@Path("order_id") int orderId,
                                             @Body Order order);

    @DELETE("order_items/{order_item_id}")
    Single<ResponseEntity<BaseEntity>> deleteOrderItem(@Path("order_item_id") int orderItemId);

    @GET("orders")
    Single<ResponseEntity<List<Order>>> getMyOrders();

    @PUT("orders/{order_id}")
    Single<ResponseEntity<Order>> completeOrder(@Path("order_id") int orderId, @Body Order order);
}
