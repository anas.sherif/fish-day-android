package com.anas.fishday.app;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDexApplication;

import com.anas.fishday.network.DaggerWebServiceComponent;
import com.anas.fishday.network.WebServiceComponent;
import com.anas.fishday.storage.FishDayPreferences;

/**
 * Created by Anas on 2/15/2018.
 */

public class FishDayApplication extends MultiDexApplication {

    private static Context context;
    private static WebServiceComponent webServiceComponent;
    @Override
    public void onCreate() {
        super.onCreate();
        context = getApplicationContext();

        webServiceComponent = DaggerWebServiceComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();

        FishDayPreferences.getInsatnce(this);
    }

    public static Context getContext(){
        return context;
    }

    public static WebServiceComponent getWebServiceComponent() {
        return webServiceComponent;
    }
}
